import 'dart:ui';

class AppColors {
  static const  Color mainBackground = Color.fromARGB(255, 66, 165, 245);
  static const  Color scaffoldBody = Color.fromARGB(255, 48,48,48);
  static const Color appBarText = Color(0xFFFFFFFF);

  static const Color personCardBackground = Color(0xFF3A3B3B);
  static const Color personCardGrayText = Color(0xFF959696);
}