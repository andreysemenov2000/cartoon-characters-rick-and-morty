import 'package:internet_connection_checker/internet_connection_checker.dart';

abstract class ConnectionStatus {
  Future<bool> get isConnected;
}

class ConnectionStatusImpl implements ConnectionStatus {
  @override
  Future<bool> get isConnected async => await InternetConnectionChecker().hasConnection;
}