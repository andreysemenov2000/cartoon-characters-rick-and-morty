import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rick_and_morty/feature/presentation/bloc/search_person_bloc/search_person.state.dart';
import 'package:rick_and_morty/feature/presentation/bloc/search_person_bloc/search_person_bloc.dart';
import 'package:rick_and_morty/feature/presentation/bloc/search_person_bloc/search_person_event.dart';
import 'package:rick_and_morty/feature/presentation/widgets/loading_indicator_widget.dart';
import 'package:rick_and_morty/feature/presentation/widgets/person_card_widget.dart';

class CustomSearchDelegate extends SearchDelegate {
  var _suggestions = ['Rick', 'Morty', 'Jerry', 'Summer'];

  // Кнопки справа вверху
  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
          icon: Icon(Icons.clear),
          onPressed: () {
            query = '';
            showSuggestions(context);
          })
    ];
  }

  // Кнопки слева вверху (назад)
  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
        icon: Icon(Icons.arrow_back),
        tooltip: 'Back',
        onPressed: () => close(context, null));
  }

  // При нажатии на кнопку поиска
  @override
  Widget buildResults(BuildContext context) {
    BlocProvider.of<SearchPersonBloc>(context)
        .add(SearchPersonStarted(personName: query));

    if (!_suggestions.contains(query)) {
      _suggestions.insert(0, query);
      if (_suggestions.length > 4) {
        _suggestions.removeLast();
      }
    }
    return BlocBuilder<SearchPersonBloc, SearchPersonState>(
        builder: (context, state) {
      if (state is SearchPersonEmpty) {
        return Container();
      }
      if (state is SearchPersonLoading) {
        return LoadingIndicatorWidget();
      }
      if (state is SearchPersonError) {
        return Center(
            child: Text(state.message,
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)));
      }
      return ListView.separated(
          itemBuilder: (context, index) =>
              PersonCardWidget((state as SearchPersonComplete).persons[index]),
          separatorBuilder: (context, index) => SizedBox(height: 16),
          itemCount: (state as SearchPersonComplete).persons.length);
    });
  }

  // Подсказки при вводе в поисковую строку
  @override
  Widget buildSuggestions(BuildContext context) {
    if (query.length != 0) return Container();
    return ListView.separated(
        itemBuilder: (context, index) => TextButton(
              onPressed: () {
                query = _suggestions[index];
                showResults(context);
              },
              child: Text(_suggestions[index], textAlign: TextAlign.left),
              style: TextButton.styleFrom(primary: Colors.white),
            ),
        separatorBuilder: (context, index) => SizedBox(),
        itemCount: _suggestions.length);
  }
}
