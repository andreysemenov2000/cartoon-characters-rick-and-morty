import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rick_and_morty/common/app_colors.dart';
import 'package:rick_and_morty/feature/domain/entities/person_entity.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:rick_and_morty/feature/presentation/pages/person_detail_page.dart';
import 'package:rick_and_morty/feature/presentation/widgets/person_cache_image.dart';

class PersonCardWidget extends StatelessWidget {
  final PersonEntity person;

  const PersonCardWidget(this.person) : super();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => PersonDetailPage(person: person)));
      },
      child: Container(
        margin: EdgeInsets.all(3),
        decoration: BoxDecoration(
            color: AppColors.personCardBackground,
            borderRadius: BorderRadius.all(Radius.circular(8))),
        child: Row(
          children: [
            Container(
                padding: EdgeInsets.only(left: 6, right: 10, top: 6, bottom: 6),
                child: PersonCacheImage(
                    imageUrl: person.image, width: 144, height: 144)
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    person.name,
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(height: 5),
                  Row(
                    children: [
                      Container(
                        width: 8,
                        height: 9,
                        decoration: BoxDecoration(
                          color: person.status == 'Alive'
                              ? Colors.green
                              : person.status == 'Dead'
                              ? Colors.red
                              : Colors.deepOrangeAccent,
                          borderRadius: BorderRadius.circular(9),
                        ),
                      ),
                      SizedBox(width: 4),
                      Expanded(
                        child: Text(
                          '${person.status} - ${person.species} (${person
                              .gender})',
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 8),
                  Text('Last known location:',
                      style: TextStyle(color: AppColors.personCardGrayText)),
                  Text(
                    person.location.name,
                  ),
                  const SizedBox(height: 8),
                  Text('Origin:',
                      style: TextStyle(color: AppColors.personCardGrayText)),
                  Text(person.origin.name),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
