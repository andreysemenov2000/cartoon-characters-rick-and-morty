//import 'dart:html';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rick_and_morty/feature/domain/entities/person_entity.dart';
import 'package:rick_and_morty/feature/presentation/bloc/person_list_cubit/person_list_cubit.dart';
import 'package:rick_and_morty/feature/presentation/bloc/person_list_cubit/person_list_state.dart';
import 'package:rick_and_morty/feature/presentation/widgets/loading_indicator_widget.dart';
import 'package:rick_and_morty/feature/presentation/widgets/person_card_widget.dart';

class PersonsListWidget extends StatelessWidget {
  final scrollController = ScrollController();

  void setupScrollController(BuildContext context) {
    scrollController.addListener(() {
      if (scrollController.position.atEdge) {
        if (scrollController.position.pixels != 0) {
          BlocProvider.of<PersonListCubit>(context).loadPersonsList();
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    List<PersonEntity> persons = [];
    setupScrollController(context);
    var isLoading = false;

    return BlocBuilder<PersonListCubit, PersonListState>(
        builder: (context, state) {
      if (state is PersonListLoading && state.isFirstFetch) {
        return Container(child: LoadingIndicatorWidget());
      } else if (state is PersonListLoading) {
          persons = state.oldPersons;
          isLoading = true;
      } else if (state is PersonListLoad) {
          persons = state.persons;
      } else if (state is PersonListError) {
          return Center(
            child: Text(state.message,
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold)),
          );
      }
      return ListView.separated(
          controller: scrollController,
          itemBuilder: (context, index) {
            if (index < persons.length) {
              print('index: $index\n List length: ${persons.length}');
              return PersonCardWidget(persons[index]);
            } else
              return LoadingIndicatorWidget();
          },
          separatorBuilder: (context, index) => SizedBox(height: 16),
          itemCount: persons.length + (isLoading ? 1: 0));
    });
  }
}
