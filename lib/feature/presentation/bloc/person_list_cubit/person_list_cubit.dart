import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rick_and_morty/cor/error/failure.dart';
import 'package:rick_and_morty/feature/domain/entities/person_entity.dart';
import 'package:rick_and_morty/feature/domain/use_cases/get_all_persons.dart';
import 'package:rick_and_morty/feature/presentation/bloc/person_list_cubit/person_list_state.dart';

const String SERVER_FAILURE_MASSAGE = 'Server Failure';
const String CACHE_FAILURE_MESSAGE = 'Cache Failure';
const String UNKNOWN_ERROR_MESSAGE =
    'Unknown error while searching for persons';

class PersonListCubit extends Cubit<PersonListState> {
  final GetAllPersons getAllPersons;
  int page = 1;

  PersonListCubit({required this.getAllPersons}) : super(PersonListEmpty());

  void loadPersonsList() async {
    if (state is PersonListLoading) return;

    final PersonListState currentState = state;

    var oldPersons = <PersonEntity>[];
    if (currentState is PersonListLoad) {
      oldPersons = currentState.persons;
    }

    emit(PersonListLoading(oldPersons, isFirstFetch: page == 1));

    final failureOrPersons = await getAllPersons(PagePersonParams(page: page));
    failureOrPersons.fold(
        (failure) => emit(PersonListError(message: _mapFailureToMessage(failure))),
        (receivedCharacters) => _getAllPersons(receivedCharacters));
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return SERVER_FAILURE_MASSAGE;
      case CacheFailure:
        return CACHE_FAILURE_MESSAGE;
      default:
        return UNKNOWN_ERROR_MESSAGE;
    }
  }

  void _getAllPersons(List<PersonEntity> receivedCharacters) {
    page++;
    final allPersons = (state as PersonListLoading).oldPersons;
    allPersons.addAll(receivedCharacters);
    emit(PersonListLoad(persons: allPersons));
  }
}
