import 'package:rick_and_morty/feature/domain/entities/person_entity.dart';

abstract class PersonListState {}

class PersonListEmpty extends PersonListState {}

class PersonListLoading extends PersonListState {
  final List<PersonEntity> oldPersons;
  final bool isFirstFetch;

  PersonListLoading(this.oldPersons, {this.isFirstFetch = false});
}

class PersonListLoad extends PersonListState {
  final List<PersonEntity> persons;

  PersonListLoad({required this.persons});
}

class PersonListError extends PersonListState {
  final String message;

  PersonListError({required this.message});
}