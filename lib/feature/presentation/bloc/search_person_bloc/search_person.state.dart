import 'package:rick_and_morty/feature/domain/entities/person_entity.dart';

abstract class SearchPersonState {}

class SearchPersonEmpty extends SearchPersonState {}

class SearchPersonLoading extends SearchPersonState {}

class SearchPersonComplete extends SearchPersonState {
  final List<PersonEntity> persons;

  SearchPersonComplete(this.persons);
}

class SearchPersonError extends SearchPersonState {
  final String message;

  SearchPersonError({required this.message});
}