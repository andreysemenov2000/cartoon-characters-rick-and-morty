abstract class SearchPersonEvent {}

class SearchPersonStarted extends SearchPersonEvent {
  final String personName;

  SearchPersonStarted({required this.personName});
}