import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rick_and_morty/cor/error/failure.dart';
import 'package:rick_and_morty/feature/domain/use_cases/search_person.dart';
import 'package:rick_and_morty/feature/presentation/bloc/search_person_bloc/search_person.state.dart';
import 'package:rick_and_morty/feature/presentation/bloc/search_person_bloc/search_person_event.dart';

const String SERVER_FAILURE_MASSAGE = 'Server Failure';
const String CACHE_FAILURE_MESSAGE = 'Cache Failure';
const String UNKNOWN_ERROR_MESSAGE =
    'Unknown error while searching for persons';

class SearchPersonBloc extends Bloc<SearchPersonEvent, SearchPersonState> {
  final SearchPerson searchPerson;

  SearchPersonBloc({required this.searchPerson}) : super(SearchPersonEmpty());

  @override
  Stream<SearchPersonState> mapEventToState(SearchPersonEvent event) async* {
    if (event is SearchPersonStarted) {
      yield SearchPersonLoading();
      var failureOrPersons =
          await searchPerson(PersonNameParams(personName: event.personName));
      // fold- библиотека dartz
      yield failureOrPersons.fold(
          (failure) =>
              SearchPersonError(message: _mapFailureToMessage(failure)),
          (persons) => SearchPersonComplete(persons));
    }
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return SERVER_FAILURE_MASSAGE;
      case CacheFailure:
        return CACHE_FAILURE_MESSAGE;
      default:
        return UNKNOWN_ERROR_MESSAGE;
    }
  }
}
