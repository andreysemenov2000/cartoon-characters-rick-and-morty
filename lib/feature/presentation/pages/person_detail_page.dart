import 'package:flutter/material.dart';
import 'package:rick_and_morty/common/app_colors.dart';
import 'package:rick_and_morty/feature/domain/entities/person_entity.dart';
import 'package:rick_and_morty/feature/presentation/widgets/person_cache_image.dart';

class PersonDetailPage extends StatelessWidget {
  final PersonEntity person;
  final textSize = 18.0;

  const PersonDetailPage({Key? key, required this.person}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.pop(context);
              })),
      body: Center(
        child: SingleChildScrollView(
          padding: EdgeInsets.symmetric(vertical: 8, horizontal: 8),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                person.name,
                textAlign: TextAlign.center,
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 32),
              ),
              SizedBox(height: 10),
              PersonCacheImage(imageUrl: person.image, width: 200, height: 200),
              SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    height: 8,
                    width: 8,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        color: person.status == "Alive"
                            ? Colors.green
                            : person.status == "unknown"
                                ? Colors.orange
                                : Colors.red),
                  ),
                  SizedBox(width: 3),
                  Text(person.status,
                      style: TextStyle(
                          fontWeight: FontWeight.w400, fontSize: textSize)),
                ],
              ),
              SizedBox(height: 30),
              Text('Species:',
                  style: TextStyle(
                      fontWeight: FontWeight.w300,
                      fontSize: textSize,
                      color: AppColors.personCardGrayText)),
              SizedBox(height: 8),
              Text(person.species,
                  style:
                      TextStyle(fontWeight: FontWeight.w400, fontSize: textSize)),
              SizedBox(height: 10),
              Text('Gender:',
                  style: TextStyle(
                      fontWeight: FontWeight.w300,
                      fontSize: textSize,
                      color: AppColors.personCardGrayText)),
              SizedBox(height: 8),
              Text(person.gender,
                  style:
                      TextStyle(fontWeight: FontWeight.w400, fontSize: textSize)),
              SizedBox(height: 10),
              Text('Last known location:',
                  style: TextStyle(
                      fontWeight: FontWeight.w300,
                      fontSize: textSize,
                      color: AppColors.personCardGrayText)),
              SizedBox(height: 8),
              Text(person.location.name,
                  style:
                      TextStyle(fontWeight: FontWeight.w400, fontSize: textSize)),
              SizedBox(height: 10),
              Text('Origin:',
                  style: TextStyle(
                      fontWeight: FontWeight.w300,
                      fontSize: textSize,
                      color: AppColors.personCardGrayText)),
              SizedBox(height: 8),
              Text(person.origin.name,
                  style:
                      TextStyle(fontWeight: FontWeight.w400, fontSize: textSize)),
              SizedBox(height: 10),
              Text('Number of episodes:',
                  style: TextStyle(
                      fontWeight: FontWeight.w300,
                      fontSize: textSize,
                      color: AppColors.personCardGrayText)),
              SizedBox(height: 8),
              Text(person.episode.length.toString(),
                  style:
                      TextStyle(fontWeight: FontWeight.w400, fontSize: textSize)),
              SizedBox(height: 10),
              Text('Was created:',
                  style: TextStyle(
                      fontWeight: FontWeight.w300,
                      fontSize: textSize,
                      color: AppColors.personCardGrayText)),
              SizedBox(height: 8),
              Text(
                  '${person.created.day}.${person.created.month}.${person.created.year}',
                  style:
                      TextStyle(fontWeight: FontWeight.w400, fontSize: textSize)),
            ],
          ),
        ),
      ),
    );
  }
}
