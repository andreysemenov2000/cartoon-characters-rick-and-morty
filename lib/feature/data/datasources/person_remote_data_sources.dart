import 'dart:convert';

import 'package:rick_and_morty/cor/error/exception.dart';
import 'package:rick_and_morty/feature/data/models/person_model.dart';
import 'package:http/http.dart' as http;

abstract class PersonRemoteDataSources {
  Future<List<PersonModel>> getAllPersons(int page);

  Future<List<PersonModel>> searchPerson(String name);
}

class PersonRemoteDataSourcesImpl implements PersonRemoteDataSources {
  final http.Client client;

  PersonRemoteDataSourcesImpl({required this.client});

  @override
  Future<List<PersonModel>> getAllPersons(int page) => _getPersonsRequest(
      'https://rickandmortyapi.com/api/character/?page=$page');

  @override
  Future<List<PersonModel>> searchPerson(String personName) => _getPersonsRequest(
      'https://rickandmortyapi.com/api/character/?name=$personName&?page=3');

  Future<List<PersonModel>> _getPersonsRequest(String url) async {
    var response = await client.get(Uri.parse(url),
        headers: {'Content-type': 'application/json'}); //ответ в формате JSON
    if (response.statusCode == 200) {
      final Map<String, dynamic> persons = json.decode(response.body);
      return (persons['results'] as List<dynamic>)
          .map((person) => PersonModel.fromJson(person))
          .toList();
    } else {
      throw ServerException();
    }
  }

}
