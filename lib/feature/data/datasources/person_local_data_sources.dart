import 'dart:convert';

import 'package:rick_and_morty/cor/error/exception.dart';
import 'package:rick_and_morty/feature/data/models/person_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class PersonLocalDataSources {
  Future<List<PersonModel>> getLastPersonsFromCache();

  Future<void> personsToCache(List<PersonModel> persons);
}

const String LAST_CACHE_PERSONS = 'LAST_CACHE_PERSONS';

class PersonLocalDataSourcesImpl implements PersonLocalDataSources {
  final SharedPreferences sharedPreferences;

  PersonLocalDataSourcesImpl({required this.sharedPreferences});

  @override
  Future<List<PersonModel>> getLastPersonsFromCache() {
    List<String>? jsonStringPersonsList =
        sharedPreferences.getStringList(LAST_CACHE_PERSONS);
    if (jsonStringPersonsList != null && jsonStringPersonsList.isNotEmpty) {
      List<PersonModel> personsModelList = jsonStringPersonsList
          .map((person) => PersonModel.fromJson(json.decode(person)))
          .toList();
      return Future.value(personsModelList);
    } else
      throw CacheException();
  }

  @override
  Future<void> personsToCache(List<PersonModel> persons) {
    List<String> jsonPersonsList =
        persons.map((person) => json.encode(person.toJson())).toList();
    sharedPreferences.setStringList(LAST_CACHE_PERSONS, jsonPersonsList);
    print('Application cache ${jsonPersonsList.length} persons');
    return Future.value(jsonPersonsList);
  }
}
