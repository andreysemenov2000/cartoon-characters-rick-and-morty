import 'package:rick_and_morty/feature/domain/entities/person_entity.dart';

class LocationModel extends LocationEntity {
  LocationModel(String name, String url) : super(name, url);

  factory LocationModel.fromJson(Map<String, dynamic> json) {
    return LocationModel(json['name'], json['url']);
  }

  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'url': url
    };
  }
}