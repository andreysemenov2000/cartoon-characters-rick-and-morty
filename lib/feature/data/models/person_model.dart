import 'package:rick_and_morty/feature/data/models/location_model.dart';
import 'package:rick_and_morty/feature/domain/entities/person_entity.dart';

class PersonModel extends PersonEntity {
  PersonModel(
      {required id,
      required name,
      required status,
      required species,
      required type,
      required gender,
      required location,
      required origin,
      required image,
      required episode,
      required created})
      : super(
            id: id,
            name: name,
            status: status,
            species: species,
            type: type,
            gender: gender,
            location: location,
            origin: origin,
            image: image,
            episode: episode,
            created: created);

  factory PersonModel.fromJson(Map<String, dynamic> json) {
    return PersonModel(
        id: json['id'],
        name: json['name'],
        status: json['status'],
        species: json['species'],
        type: json['type'],
        gender: json['gender'],
        location: json['location'] != null
            ? LocationModel.fromJson(json['location'])
            : LocationModel('Неизвестно', 'null'),
        origin: json['origin'] != null
            ? LocationModel.fromJson(json['origin'])
            : LocationModel('Неизвестно', 'null'),
        image: json['image'],
        episode:
            (json['episode'] as List<dynamic>).map((e) => e as String).toList(),
        created: DateTime.parse(
            json['created'] as String)
        );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
      'status': status,
      'species': species,
      'type': type,
      'gender': gender,
      'location': location,
      'origin': origin,
      'image': image,
      'episode': episode,
      'created': created.toIso8601String()
    };
  }
}
