import 'package:dartz/dartz.dart';
import 'package:rick_and_morty/cor/error/exception.dart';
import 'package:rick_and_morty/cor/error/failure.dart';
import 'package:rick_and_morty/cor/platform/connection_status.dart';
import 'package:rick_and_morty/feature/data/datasources/person_local_data_sources.dart';
import 'package:rick_and_morty/feature/data/datasources/person_remote_data_sources.dart';
import 'package:rick_and_morty/feature/data/models/person_model.dart';
import 'package:rick_and_morty/feature/domain/entities/person_entity.dart';
import 'package:rick_and_morty/feature/domain/repositories/person_repository.dart';

class PersonRepositoryImpl implements PersonRepository {
  final ConnectionStatus connectionStatus;
  final PersonRemoteDataSources personRemoteDataSources;
  final PersonLocalDataSources personLocalDataSources;

  PersonRepositoryImpl(
      {required this.connectionStatus,
      required this.personRemoteDataSources,
      required this.personLocalDataSources});

  @override
  Future<Either<Failure, List<PersonEntity>>> getAllPersons(int page) async {
    return _getPersons(() {
      return personRemoteDataSources.getAllPersons(page);
    });
  }

  @override
  Future<Either<Failure, List<PersonEntity>>> searchPersons(String personName) {
    return _getPersons(() {
      return personRemoteDataSources.searchPerson(personName);
    });
  }

  Future<Either<Failure, List<PersonEntity>>> _getPersons(
      Future<List<PersonModel>> Function() getOrSearchPersons) async {
    if (await connectionStatus.isConnected) {
      try {
        List<PersonModel> remotePersons = await getOrSearchPersons();
        personLocalDataSources.personsToCache(remotePersons);
        return Right(remotePersons);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      try {
        List<PersonModel> cachePersons =
            await personLocalDataSources.getLastPersonsFromCache();
        return Right(cachePersons);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }
}
