import 'package:dartz/dartz.dart';
import 'package:rick_and_morty/cor/error/failure.dart';
import 'package:rick_and_morty/feature/domain/entities/person_entity.dart';

abstract class PersonRepository {
  Future<Either<Failure, List<PersonEntity>>> getAllPersons(int page);
  Future<Either<Failure, List<PersonEntity>>> searchPersons(String personName);
}