import 'package:dartz/dartz.dart';
import 'package:rick_and_morty/cor/error/failure.dart';
import 'package:rick_and_morty/cor/use_case_base/use_case.dart';
import 'package:rick_and_morty/feature/domain/entities/person_entity.dart';
import 'package:rick_and_morty/feature/domain/repositories/person_repository.dart';

class SearchPerson extends UseCase<List<PersonEntity>, PersonNameParams> {
  final PersonRepository personRepository;

  SearchPerson(this.personRepository);

  @override
  Future<Either<Failure, List<PersonEntity>>> call(PersonNameParams params) async{
    return await personRepository.searchPersons(params.personName);
  }
}

class PersonNameParams {
  final String personName;

  PersonNameParams({required this.personName});
}