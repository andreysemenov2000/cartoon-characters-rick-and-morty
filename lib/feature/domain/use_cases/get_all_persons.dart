import 'package:dartz/dartz.dart';
import 'package:rick_and_morty/cor/error/failure.dart';
import 'package:rick_and_morty/cor/use_case_base/use_case.dart';
import 'package:rick_and_morty/feature/domain/entities/person_entity.dart';
import 'package:rick_and_morty/feature/domain/repositories/person_repository.dart';

class GetAllPersons extends UseCase<List<PersonEntity>, PagePersonParams> {
  final PersonRepository personRepository;

  GetAllPersons(this.personRepository);

  @override
  Future<Either<Failure, List<PersonEntity>>> call(PagePersonParams params) async {
    return await personRepository.getAllPersons(params.page);
  }
}

class PagePersonParams {
  final int page;

  PagePersonParams({required this.page});
}