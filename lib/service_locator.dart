import 'package:get_it/get_it.dart';
import 'package:rick_and_morty/cor/platform/connection_status.dart';
import 'package:rick_and_morty/feature/data/datasources/person_local_data_sources.dart';
import 'package:rick_and_morty/feature/data/datasources/person_remote_data_sources.dart';
import 'package:rick_and_morty/feature/data/repositories/person_repository_impl.dart';
import 'package:rick_and_morty/feature/domain/repositories/person_repository.dart';
import 'package:rick_and_morty/feature/domain/use_cases/get_all_persons.dart';
import 'package:rick_and_morty/feature/domain/use_cases/search_person.dart';
import 'package:rick_and_morty/feature/presentation/bloc/person_list_cubit/person_list_cubit.dart';
import 'package:rick_and_morty/feature/presentation/bloc/search_person_bloc/search_person_bloc.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

final sl = GetIt.instance;

Future<void> slInit() async {
  // BLoC, Cubit
  sl.registerFactory(() => PersonListCubit(getAllPersons: sl()));
  sl.registerFactory(() => SearchPersonBloc(searchPerson: sl()));

  // Use Case
  sl.registerLazySingleton(() => GetAllPersons(sl()));
  sl.registerLazySingleton(() => SearchPerson(sl()));

  // Repository
  sl.registerLazySingleton<PersonRepository>(() => PersonRepositoryImpl(
      connectionStatus: sl(),
      personRemoteDataSources: sl(),
      personLocalDataSources: sl()));

  // Data sources
  sl.registerLazySingleton<ConnectionStatus>(() => ConnectionStatusImpl());
  sl.registerLazySingleton<PersonRemoteDataSources>(
      () => PersonRemoteDataSourcesImpl(client: http.Client()));
  sl.registerLazySingleton<PersonLocalDataSources>(
      () => PersonLocalDataSourcesImpl(sharedPreferences: sl()));

  // External
  final sharedPreferences = await SharedPreferences.getInstance();
  sl.registerLazySingleton(() => sharedPreferences);
  sl.registerLazySingleton(() => http.Client());
}
