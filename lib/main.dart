import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rick_and_morty/feature/presentation/bloc/person_list_cubit/person_list_cubit.dart';
import 'package:rick_and_morty/feature/presentation/bloc/search_person_bloc/search_person_bloc.dart';
import 'package:rick_and_morty/service_locator.dart';

import 'common/app_colors.dart';
import 'feature/presentation/pages/persons_list_page.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await slInit();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<PersonListCubit>(
            create: (context) => sl<PersonListCubit>()..loadPersonsList()),
        BlocProvider<SearchPersonBloc>(
            create: (context) => sl<SearchPersonBloc>())
      ],
      child: MaterialApp(
        title: 'Rick and Morty',
        theme: ThemeData.dark().copyWith(
          backgroundColor: AppColors.mainBackground,
          scaffoldBackgroundColor: AppColors.scaffoldBody,
        ),
        home: PersonListPage(),
      ),
    );
  }
}
